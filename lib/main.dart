import 'dart:math';

import 'package:flutter/material.dart';
import '/vue/connexion.dart';
import '/vue/accueil.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // Définir la couleur principale avec MaterialColor
  MaterialColor couleurPrincipale = MaterialColor(0xFF5B2333, {
    50: Color.fromRGBO(91, 35, 51, 0.1),
    100: Color.fromRGBO(91, 35, 51, 0.2),
    200: Color.fromRGBO(91, 35, 51, 0.3),
    300: Color.fromRGBO(91, 35, 51, 0.4),
    400: Color.fromRGBO(91, 35, 51, 0.5),
    500: Color.fromRGBO(91, 35, 51, 0.6),
    600: Color.fromRGBO(91, 35, 51, 0.7),
    700: Color.fromRGBO(91, 35, 51, 0.8),
    800: Color.fromRGBO(91, 35, 51, 0.9),
    900: Color.fromRGBO(91, 35, 51, 1.0),
  });

  MaterialColor couleurSecondaire = MaterialColor(0xFFF7F4F3, {
    50: Color.fromRGBO(247, 244, 243, 0.1),
    100: Color.fromRGBO(247, 244, 243, 0.2),
    200: Color.fromRGBO(247, 244, 243, 0.3),
    300: Color.fromRGBO(247, 244, 243, 0.4),
    400: Color.fromRGBO(247, 244, 243, 0.5),
    500: Color.fromRGBO(247, 244, 243, 0.6),
    600: Color.fromRGBO(247, 244, 243, 0.7),
    700: Color.fromRGBO(247, 244, 243, 0.8),
    800: Color.fromRGBO(247, 244, 243, 0.9),
    900: Color.fromRGBO(247, 244, 243, 1.0),
  });

  MaterialColor couleurTertiaire = MaterialColor(0xFF564D4A, {
    50: Color.fromRGBO(86, 77, 74, 0.1),
    100: Color.fromRGBO(86, 77, 74, 0.2),
    200: Color.fromRGBO(86, 77, 74, 0.3),
    300: Color.fromRGBO(86, 77, 74, 0.4),
    400: Color.fromRGBO(86, 77, 74, 0.5),
    500: Color.fromRGBO(86, 77, 74, 0.6),
    600: Color.fromRGBO(86, 77, 74, 0.7),
    700: Color.fromRGBO(86, 77, 74, 0.8),
    800: Color.fromRGBO(86, 77, 74, 0.9),
    900: Color.fromRGBO(86, 77, 74, 1.0),
  });

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Cinéphoria',
      theme: ThemeData(
        primarySwatch: couleurPrincipale,
        scaffoldBackgroundColor: couleurSecondaire,
      ),
       home: Scaffold(
        appBar: AppBar(
          backgroundColor: couleurSecondaire,
        ),
        body: ConnexionPage(),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
