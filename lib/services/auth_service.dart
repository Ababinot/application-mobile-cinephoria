import 'package:flutter/material.dart';
import 'package:flutter_cinephoria/vue/accueil.dart';
import 'package:flutter_cinephoria/vue/connexion.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class AuthService {
  bool _isConnected = false;
  String? _userEmail;

  Future<void> login(BuildContext context, String email, String password,
      GlobalKey<FormState> formKey) async {
    if (formKey.currentState!.validate()) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('Connexion en cours...')),
      );

      final response = await http.post(
        Uri.parse('https://apicinephoria.fly.dev/api/login'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          'email': email,
          'password': password,
        }),
      );

      if (response.statusCode == 200) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text('Connexion réussie!')),
        );
        _isConnected = true;
        _userEmail = email;
        Navigator.of(context).pushReplacement(
          MaterialPageRoute(
            builder: (context) => Accueil(email: email),
          ),
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text('Erreur de connexion: ${response.body}')),
        );
      }
    }
  }

  void logout(BuildContext context) {
    _isConnected = false;
    _userEmail = null;
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(content: Text('Vous avez été déconnecté(e) avec succès')),
    );
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) => ConnexionPage(),
      ),
    );
  }

  String? getUserEmail() {
    return _userEmail;
  }
}
