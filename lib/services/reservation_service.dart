import 'package:http/http.dart' as http;
import 'dart:convert';
import '/models/reservations.dart';

class ReservationService {
  Future<List<Reservation>> getReservations(String email) async {
    final response = await http.get(
      Uri.parse('https://apicinephoria.fly.dev/api/espace-utilisateur-mobile/$email'),
    );

    if (response.statusCode == 200) {
      List<dynamic> jsonReservations = jsonDecode(response.body);
      List<Reservation> reservations = jsonReservations
    .map((json) => Reservation.fromJson(json))
    .where((reservation) => reservation.date != null && reservation.date!.isAfter(DateTime.now()))
    .toList();

      return reservations;
    } else {
      throw Exception('Erreur lors de la récupération des réservations: ${response.body}');
    }
  }
}
