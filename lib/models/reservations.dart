class Reservation {
  final int id;
  final String titre;
  final String image;
  final DateTime? date;
  final String heureDebut;
  final String heureFin;
  final int numeroSalle;
  final int nombre_personnes;

  Reservation({
    required this.id,
    required this.titre,
    required this.image,
    required this.date,
    required this.heureDebut,
    required this.heureFin,
    required this.numeroSalle,
    required this.nombre_personnes,
  });

  factory Reservation.fromJson(Map<String, dynamic> json) {
    return Reservation(
      id: json['id_reservation'],
      titre: json['nom_film'],
      image: json['affiche_film'],
      date: json['date'] != null ? DateTime.parse(json['date']).toLocal() : null,
      heureDebut: json['heure_debut'],
      heureFin: json['heure_fin'],
      numeroSalle: json['numero_salle'],
      nombre_personnes: json['nombre_personnes'],
    );
  }
}
