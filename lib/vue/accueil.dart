import 'package:flutter/material.dart';
import '/widgets/reservation_card.dart';
import '/services/reservation_service.dart'; // Assurez-vous d'importer le service correctement
import '/widgets/custom_widget.dart';
import '/models/reservations.dart';

class Accueil extends StatelessWidget {
  final String email;
  final ReservationService reservationService = ReservationService();

  Accueil({required this.email});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFF7F4F3),
        actions: [
          BoutonDeconnexion(),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Mes réservations',
              style: TextStyle(
                fontSize: 24,
              ),
            ),
            SizedBox(height: 15),
            Text(
              'Visionner toutes vos réservations à venir',
              style: TextStyle(fontSize: 14, color: Colors.grey[600]),
              textAlign: TextAlign.left,
            ),
            SizedBox(height: 20),
            Expanded(
              child: FutureBuilder<List<Reservation>>(
                future: reservationService.getReservations(email),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Center(child: CircularProgressIndicator());
                  } else if (snapshot.hasError) {
                    return Center(
                        child: Text(
                            'Erreur de chargement des réservations: ${snapshot.error}'));
                  } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
                    return Center(child: Text('Aucune réservation trouvée'));
                  } else {
                    List<Reservation> reservations = snapshot.data!;
                    return ListView.builder(
                      itemCount: reservations.length,
                      itemBuilder: (BuildContext context, int index) {
                        Reservation reservation = reservations[index];
                        return ReservationCard(reservation: reservation);
                      },
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
