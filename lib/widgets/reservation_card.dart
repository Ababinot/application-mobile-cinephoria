import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:qr_flutter/qr_flutter.dart';
import '/models/reservations.dart';

class ReservationCard extends StatefulWidget {
  final Reservation reservation;

  const ReservationCard({required this.reservation});

  @override
  _ReservationCardState createState() => _ReservationCardState();
}

class _ReservationCardState extends State<ReservationCard> {
  bool _showQrCode = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          _showQrCode = !_showQrCode;
        });
        print('_showQrCode toggled: $_showQrCode');
      },
      child: Card(
        color: Colors.white,
        elevation: 4,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (widget.reservation.image != null)
                Image.network(
                  widget.reservation.image!,
                  width: double.infinity,
                  height: 200,
                  fit: BoxFit.cover,
                ),
              SizedBox(height: 16),
              Text(
                widget.reservation.titre,
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 8),
              Text(
                widget.reservation.date != null
                    ? 'Le ${DateFormat('dd/MM/yyyy').format(widget.reservation.date!)}'
                    : '',
              ),
              SizedBox(height: 8),
              Text('Salle: numéro ${widget.reservation.numeroSalle ?? ''}'),
              SizedBox(height: 8),
              Text(
                '${widget.reservation.heureDebut ?? ''} - ${widget.reservation.heureFin ?? ''}',
              ),
              SizedBox(height: 16),
              if (_showQrCode)
                Center(
                  child: QrImageView(
                    data: 'Réservation: ${widget.reservation.id}, Nombre de personnes : ${widget.reservation.nombre_personnes}',
                    version: QrVersions.auto,
                    size: 250.0,
                    errorCorrectionLevel: QrErrorCorrectLevel.L,
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
