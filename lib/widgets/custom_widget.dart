import 'package:flutter/material.dart';
import '/services/auth_service.dart';

class BoutonDeconnexion extends StatelessWidget {
  final AuthService _authService = AuthService();
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: TextButton(
        onPressed: () {
          _authService.logout(context);
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text('Vous avez été déconnecté(e) avec succès')),
          );
        },
        style: TextButton.styleFrom(
          backgroundColor: Color(0xFF5B2333),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
        child: Text(
          'Déconnexion',
          style: TextStyle(fontSize: 13, color: Color(0xFFF7F4F3)),
        ),
      ),
    );
  }
}
